/*
Task:
Write a program that asks the user for two integers and a character,
'A', 'S', or 'M'. Call one of three funnctions that adds, subtracts
or mulitplies the user's integers based on the character input.
*/

#include<iostream>
#include<stdlib.h>
using namespace std;

//Create our class and give it any name
class commissionOperations{
    //declare a public set of functions inside it
public:
    void getCommision(double sales); //Check vaue of commission
    //Calculate commission and otput it
    void commission1(double sales, double commission); 
    void commission2(double sales, double commission);
    void commission3(double sales, double commission);
};

//Main function
int main(){
    //Create variable for our user input
    double sales;
    cout << "Enter value of sales:" << endl;
    cin >> sales;

    //call the class we created as findCommission
    commissionOperations findCommission;

    //Check value of sales
    findCommission.getCommision(sales);
    return 0;
    }

//Call our function from our class and pass the user input as sales
void commissionOperations

::getCommision(double sales){
    //Check the value of the commission
    double commission = 0;
    if (sales < 1000){
        //Call function to get commision
        commission1(sales, commission);
    }
    else if(sales < 3000){
        //Call function to get commision
        commission2(sales, commission);
    }
    else if(sales >= 3000){
        //Call function to get commision
        commission3(sales,commission);
    }
}
/*Call our function from our class and pass the user
input as sales with commission initialized to 0*/
void commissionOperations
::commission1(double sales, double firstCommission){
    //Find commission and output it
    firstCommission = sales * 0.03;
    cout << "The total commission for "<< sales << " sales is KSH" << firstCommission << endl;
}
/*Call our function from our class and pass the user
input as sales with commission initialized to 0*/
void commissionOperations
::commission2(double sales, double secondCommission){
    //Find commission and output it
    secondCommission = sales * 0.035;
    cout << "The total commission for "<< sales << " sales is KSH" << secondCommission << endl;
}
/*Call our function from our class and pass the user
input as sales with commission initialized to 0*/
void commissionOperations
::commission3(double sales, double thirdCommission){
    //Find commission and output it
    thirdCommission = sales * 0.045;
    cout << "The total commission for "<< sales << " sales is KSH" << thirdCommission << endl;
}