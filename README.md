# Object Oriented and Development Programs #

This is a repository to store all code snippets that I've written for this unit.
### Contents ###

* Programs sources
* Instructions for what each program does can be found inside the program file on a commented line at the top or at the program list below

### How to run the programs: ###

* Clone this repository onto your local machine
* Compile the program you want to run
* Run it!
* I used g++ on Linux and it worked fine. Should do the same with MinGW.

### Program List: ###
1. **arithmetic.cpp** - Write a program that asks the user for two integers and a character, 'A', 'S', or 'M'. Call one of three functions that adds, subtracts
or multiplies the user's integers based on the character input.

2. **sales.cpp - ** Write a program including two functions. The first function should ask a salesperson for the value of daily sales and return this figure to the main function. The second function should calculate person's interest based on the following rates{(KSH0-KSH999 gets 3% commission), (KSH1000-KSH2999 gets 3.5% commission), (KSH3000 - up gets 4.5% commission.)}

3. 

### Need something? ###

* Lewis Munyi
* lewismunyi97gmail.com