/*
Task:
Write a program including two functions. The first function should
ask a salesperson for the value of daily sales and return this figure to the main function. The second function should calculate 
person's interest based on the following rates:
1. KSH0-KSH999 gets 3% commission
2. KSH1000-KSH2999 gets 3.5% commission
3. KSH3000 - up gets 4.5% commission
*/
#include <iostream>
#include <string>


using namespace std;

//Create a clas and call it any name
class ourOperations
 {
    //Create public functions in our class
public:
    int getInt();
    void add(int fnum, int snum);
    void subtract(int fnum, int snum);
    void multiply(int fnum, int snum);
    
    };

int main()
{
    //Call our getint 
    ourOperations operation;
    operation.getInt();
        
    return 0;
}

int ourOperations
//resolve getint
::getInt() {
    //Get numbers
    int fnum, snum;
    cout << "Enter the first integer:"<< endl;
    cin >> fnum;
    cout << "Enter the second integer:" << endl;
    cin >> snum;
    
    char choice;

    //Get user choice and store it in a variable for switching
    cout << "Enter the Operation you want to perform :\n A. Add\n S. Subtract\n M. Multiply" << endl;
    cin >> choice;
    //Switch choice
    switch (choice)
    {
    case 'A':
        cout << "Addition" << endl;
        //call function add
        add(fnum, snum);
        break;
    case 'S':
        cout << "Subtraction" << endl;
        //call function subtract
        subtract(fnum, snum);
        break;
    case 'M':
        cout << "Multiplication" << endl;
        //Call function mulitplication
        multiply(fnum, snum);
        break;

    default:
    //Output error
        cout << "Invalid choice" << endl;
        break;
    }
    //Exit
    return (0);
    
}

void ourOperations
//function add
::add(int fnum, int snum) {
    double sum;
    sum = fnum + snum;
    cout << "The sum of " << fnum << " and " << snum << " is " << sum << endl;
}
//function multiply
void ourOperations
::multiply(int fnum, int snum) {
    double product;
    product = fnum * snum;
    cout << "The product of " << fnum << " and " << snum << " is " << product << endl;
}

void ourOperations
//Function subtract
::subtract(int fnum, int snum) {
    double difference;
    difference = fnum * snum;
    cout << "the difference of " << fnum << " and " << snum << " is " << difference << endl;
}

